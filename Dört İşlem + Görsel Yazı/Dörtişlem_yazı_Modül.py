import time
def dörtişlem(dörtişlem):# Yapmak iistediğiniz dört işlemi giriniz küçük harfle (çarpma,bölme,toplama,çıkarma)
    try:
        #Sayı isimleri
        birlerbasamagı = ["","Birinci","İkinci","Üçüncü","Dörtüncü","Beşinci","Altıncı","Yedinci","Sekizinci","Dokuzuncu"]
        onlarbasamagı = ["","On","Yirmi","Otuz","Kırk","Elli","Altmış","Yetmiş","Seksen","Doksan","Yüz","Yüz On","Yüz Yirmi","Yüz Otuz","Yüz Kırk","Yüz Elli","Yüz Altmış","Yüz Yetmiş","Yüz Seksen","Yüz Doksan","İki Yüz","İki Yüz On","İki Yüz Yirmi","İki Yüz Otuz","İki Yüz Kırk","İki Yüz Elli","İki Yüz Altmış","İki Yüz Yetmiş","İki Yüz Seksen","İki Yüz Doksan"]
        sayı_ismi = [""]
        #while döngü için gerekli işlemler
        while_sayı = 1
        çarpma_işlemi_sonucu = 1
        bölme_işlemi_sonucu = 0
        toplama_işlemi_sonucu = 0
        çıkarma_işlemi_sonucu = 0
        def sonuç(a):#10 un katlarında <uncu inci> eklerinin eklenmesi için hazırlamış olduğum işlem
            birler = a % 10
            onlar = a // 10
            # 10 ve 30 ile biten katlar için 300 e kadar
            if(a == 10 or a == 30 or a == 110 or a == 130 or a == 210 or a == 230):
                sayı_ismi.append(onlarbasamagı[onlar]+"uncu")
            #20 ve 50 ile biten katlar için 300 ekadar
            elif(a == 20 or a == 50 or a == 120 or a == 150 or a == 220 or a == 250):
                sayı_ismi.append(onlarbasamagı[onlar]+"nci")
            #40 , 60 ve 90 ile biten katlar için 300 e kadar
            elif(a == 40 or a == 60 or a == 90 or a == 140 or a == 160 or a == 190 or a == 240 or a == 260 or a == 290):
                sayı_ismi.append(onlarbasamagı[onlar]+"ıncı")
            #70 ve 80 ile biten katlar için 300 e kadar
            elif(a == 70 or a == 80 or a == 170 or a == 180 or a == 270 or a == 280):
                sayı_ismi.append(onlarbasamagı[onlar]+"inci")
            #100 ile biten katlarıiçin 300 e kadar
            elif(a == 100 or a == 200 or a == 300):
                sayı_ismi.append(onlarbasamagı[onlar]+"üncü")
            else:
                sayı_ismi.append(onlarbasamagı[onlar] + " " + birlerbasamagı[birler])


        sayı_miktarı = int(input("\nKaç adet sayı "+dörtişlem+"k istersiniz : "))
        print("")
        if(sayı_miktarı > 1 and sayı_miktarı <= 300):
            while(while_sayı < sayı_miktarı+1):
                sonuç(while_sayı)

                sayı_gir = int(input(sayı_ismi[-1]+" Sayı Giriniz : "))
                if (while_sayı % 10 == 9):
                    print("_________________________________\n")
                while_sayı +=1
                if(dörtişlem == "çarpma"):
                    çarpma_işlemi_sonucu *= sayı_gir
                elif(dörtişlem == "bölme"):
                    if(while_sayı == 2):
                        bölme_işlemi_sonucu = sayı_gir
                    else:
                        bölme_işlemi_sonucu /= sayı_gir
                elif(dörtişlem == "toplama"):
                    toplama_işlemi_sonucu += sayı_gir
                elif(dörtişlem == "çıkarma"):
                    if(while_sayı == 2):
                        çıkarma_işlemi_sonucu = sayı_gir
                    else:
                        çıkarma_işlemi_sonucu -= sayı_gir

            if (dörtişlem == "çarpma"):
                print("Hesaplanıyor . . .")
                t = time.sleep(2.5)
                print("\nÇarpma İşlemi Sonucu : ", çarpma_işlemi_sonucu,"\n")
                time.sleep(2)
            elif (dörtişlem == "bölme"):
                print("Hesaplanıyor . . .")
                time.sleep(1.5)
                print("Bölme İşlemi Sonucu : ", bölme_işlemi_sonucu,"\n")
                time.sleep(2)
            elif (dörtişlem == "toplama"):
                print("Hesaplanıyor . . .")
                print("\nToplama İşlemi Sonucu : ", toplama_işlemi_sonucu,"\n")
                time.sleep(2)
            elif (dörtişlem == "çıkarma"):
                print("Hesaplanıyor . . .")
                t = time.sleep(1)
                print("\nÇıkarma İşlemi Sonucu : ", çıkarma_işlemi_sonucu,"\n")
                time.sleep(2)
        else:
            print("\nEn Fazla 300 En az 2 Sayı girebilirsiniz ..!")
            time.sleep(2)
    except ValueError:
        print("\nLütfen sadece Sayı girin ..!")










